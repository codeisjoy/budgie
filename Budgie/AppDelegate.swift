//
//  AppDelegate.swift
//  Budgie
//
//  Created by Emad A. on 6/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import Photos
import Mixpanel

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
	var permissionsViewController: UIViewController?
	
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		let environment = Environment.current.configuration
		if environment == .debug {
			Mixpanel.sharedInstance(withToken: "ceae2b8ad95b0a6253dab234a5438216")
		} else if environment == .release {
			Mixpanel.sharedInstance(withToken: "68f56d281d373740f64d302ff7e0e2af")
		}
		
        UIBarButtonItem.appearance().setTitleTextAttributes(
            [NSFontAttributeName: UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)],
            for: UIControlState.normal)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.black
        window?.tintColor = UIColor(0x4A4A4A)
        window?.makeKeyAndVisible()
        
        let nc = UINavigationController(rootViewController: HomeViewController())
        nc.navigationBar.setBackgroundImage(UIImage.solid(color: .white), for: .default)
        nc.navigationBar.shadowImage = UIImage.solid(color: UIColor(0xD0D0D0))
		nc.navigationBar.isTranslucent = false
        window?.rootViewController = nc
		
        return true
    }

	func applicationDidBecomeActive(_ application: UIApplication) {
		if PHPhotoLibrary.authorizationStatus() != .authorized {
			let vc = PermissionsViewController()
			permissionsViewController = vc
			window?.rootViewController?.present(vc, animated: false, completion: nil)
		} else {
			permissionsViewController?.dismiss(animated: true, completion: nil)
		}
    }
    
}

