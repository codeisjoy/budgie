//
//  SaveActionViewController.swift
//  Budgie
//
//  Created by Emad A on 29/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class SaveActionViewController: BaseViewController {
	
	var viewModel: SaveActionViewModel?
	
	var dismiss: (() -> ())?
	
	// MARK: - View Properties
	
	private let loadingView: LoadingView = {
		let view = LoadingView()
		view.tintColor = .white
		
		return view
	}()
	
    private let savedLabel: UILabel = {
        let label = UILabel()
        label.text = "Saved"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        
        return label
    }()
	
	private let buttonsView: UIStackView = {
		let view = UIStackView()
		view.axis = .vertical
		
		return view
	}()
	
	private let shareButton: UIButton = {
		let btn = RoundedButton(type: .system)
		btn.setImage(#imageLiteral(resourceName: "icon_instagram"), for: .normal)
		btn.setTitle("Share on Instagram", for: .normal)
		
		return btn
	}()
	
	private let startButton: UIButton = {
		let btn = RoundedButton(type: .system)
		btn.setImage(#imageLiteral(resourceName: "icon_arrow"), for: .normal)
		btn.setTitle("New Vawin", for: .normal)
		
		return btn
	}()
	
    private let feedbackGenerator = UINotificationFeedbackGenerator()
    
	private let disposeBag = DisposeBag()
	
	// MARK: -
	
	init(photo: UIImage,
	     numberOfSlices count: Int,
	     sizing: PhotoSizing,
	     format: PhotoFormat,
	     quality: PhotoQuality)
	{
		super.init(nibName: nil, bundle: nil)
		
		modalTransitionStyle = .crossDissolve
		modalPresentationStyle = .overCurrentContext
		
		viewModel = SaveActionViewModel(
			photo: photo,
			numberOfSlices: count,
			sizing: sizing,
			format: format,
			quality: quality,
			completion: { [weak self] in
				self?.feedbackGenerator.prepare()
				self?.loadingView.willComplete()
			},
			presentError: { [weak self] in
				self?.presentError(error: $0)
		})
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		let blurEffect = UIBlurEffect(style: .light)
		let blurredView = UIVisualEffectView(effect: blurEffect)
		blurredView.contentView.backgroundColor = UIColor(white: 0, alpha: 0.45)
		
		blurredView.contentView.addSubview(loadingView)
		
		savedLabel.isHidden = true
        blurredView.contentView.addSubview(savedLabel)
		
		buttonsView.addArrangedSubview(shareButton)
		buttonsView.addArrangedSubview(startButton)
		
		buttonsView.isHidden = true
		blurredView.addSubview(buttonsView)
		
		self.view = blurredView
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		loadingView.rx.didComplete
            .observeOn(MainScheduler.instance)
			.bind(onNext: { [unowned self] in
				self.savedLabel.alpha = 0
				self.savedLabel.isHidden = false
				self.savedLabel.transform = CGAffineTransform(translationX: 0, y: -10)
				
                self.feedbackGenerator.notificationOccurred(.success)
                
				UIView.animate(
					withDuration: 0.5,
					delay: 0,
					usingSpringWithDamping: 1,
					initialSpringVelocity: 1,
					options: .beginFromCurrentState,
					animations: {
						self.savedLabel.alpha = 1
						self.savedLabel.transform = .identity
					},
					completion: nil)
				
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
					self.buttonsView.alpha = 0
					self.buttonsView.isHidden = false
					self.buttonsView.transform = CGAffineTransform(translationX: 0, y: 10)
					// Because of weired behavour of stack view after changing `isHidden`
					// we need to wait until `alpha = 0` get applied.
					DispatchQueue.main.async {
						UIView.animate(
							withDuration: 0.5,
							delay: 0,
							usingSpringWithDamping: 1,
							initialSpringVelocity: 1,
							options: .beginFromCurrentState,
							animations: {
								self.buttonsView.alpha = 1
								self.buttonsView.transform = .identity
							},
							completion: nil)
					}
				}
			})
			.disposed(by: disposeBag)
		
        shareButton.rx.controlEvent(.touchUpInside)
            .subscribe(onNext: { [weak self] in
                self?.shareOnInstagram()
            })
            .disposed(by: disposeBag)
        
		startButton.rx.controlEvent(.touchUpInside)
			.subscribe(onNext: { [weak self] in
				self?.dismiss?()
				self?.dismiss(animated: true, completion: nil)
			})
			.disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
		
		loadingView.start()
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.viewModel?.export()
        }
		
		if let url = URL(string:"instagram://"), UIApplication.shared.canOpenURL(url) == false {
			shareButton.isHidden = true
		}
    }
    
    override func setupViewConstraints() {
        super.setupViewConstraints()
        
		loadingView.autoAlignAxis(toSuperviewAxis: .vertical)
		loadingView.autoAlignAxis(toSuperviewAxis: .horizontal)
		loadingView.autoSetDimensions(to: CGSize(width: 70, height: 70))
        
        savedLabel.autoAlignAxis(toSuperviewMarginAxis: .vertical)
        savedLabel.autoPinEdge(.top, to: .bottom, of: loadingView, withOffset: 15)
		
		buttonsView.autoSetDimension(.width, toSize: 200)
		buttonsView.autoAlignAxis(toSuperviewAxis: .vertical)
		buttonsView.autoAlignAxis(.horizontal, toSameAxisOf: view, withMultiplier: 1.55)
		buttonsView.spacing = 20
	}
    
    private func shareOnInstagram() {
        guard let url = URL(string: "instagram://library?LocalIdentifier"),
            UIApplication.shared.canOpenURL(url) else
        {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func presentError(error: Error) {
        let errorController = UIAlertController(
            title: "Error",
            message: "Something went wrong. Can not complete the task.",
            preferredStyle: .alert)
        
        errorController.addAction(
            UIAlertAction(title: "Cancel", style: .cancel, handler: { [weak self] _ in
                self?.dismiss(animated: true, completion: nil)
            }))
        
        loadingView.stop()
        present(errorController, animated: true, completion: nil)
    }
}
