//
//  UIColorExtension.swift
//  Budgie
//
//  Created by Emad A. on 18/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init(_ hex: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let r = CGFloat((hex & 0xFF0000) >> 16) / divisor
        let g = CGFloat((hex & 0x00FF00) >>  8) / divisor
        let b = CGFloat( hex & 0x0000FF       ) / divisor
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
}
