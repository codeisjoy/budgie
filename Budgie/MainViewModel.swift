//
//  MainViewModel.swift
//  Budgie
//
//  Created by Emad A. on 7/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import Foundation
import RxSwift
import Mixpanel

struct MainViewModel {
	
	// MARK: - Public Properties
	
	/// The original photo.
    let photo = Variable<UIImage?>(nil)
	
	/// The size of each slice.
    let sliceSize = Variable<CGSize>(.zero)
	
	/// The cropped and scaled down preview photo.
    let preview = Variable<UIImage?>(nil)
	
    /// The user settings.
    let settings: SettingsViewModel
    
    // MARK: - Private Properties
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Public Methods
    
    init(settings: SettingsViewModel) {
        self.settings = settings
		
		// Update preview photo when either original photo or slice size changed.
        Observable.merge([photo.asObservableVoid(), sliceSize.asObservableVoid()])
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
			.map { [weak photo] in return photo?.value }
            .bind(onNext: updatePreview)
            .disposed(by: disposeBag)
		
    }
	
	// MARK: - Private Methods
	
	/// Set `preview` value with updated photo based on slice sizes.
	private func updatePreview(photo: UIImage?) {
		// Make sure photo exists with appropriate dimension.
        guard let originalSize = photo?.size, originalSize.isAnyDimentionZero == false else {
			preview.value = nil
			return
        }
		
		// Determine the scale factor ...
		let sliceSize = self.sliceSize.value
		let scale = max(
			sliceSize.width  / originalSize.width,
			sliceSize.height / originalSize.height)
        // ... and make sure it is meaningful.
        guard scale > 0 else {
            preview.value = nil
            return
        }
		
        // Say how many slice could be exported based on slice size and scale factor.
		let count = max(floor(originalSize.width * scale / sliceSize.width), 1)
		// As knowing each slice size and count of them, trim and scale down the photo for preview purpose.
		let rect = CGRect(
			origin: .zero,
			size: CGSize(
				width: floor(count * (sliceSize.width / scale)),
				height: floor(sliceSize.height / scale)))
		// That's it!
		preview.value = photo?.crop(rect: rect, scale: scale)
		
		Mixpanel.sharedInstance()?.track("Preview", properties: [
			"Photo Dimension": originalSize.debugDescription,
			"Number Of Slices": count])
	}
	
}

