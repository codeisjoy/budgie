//
//  SettingsViewModel.swift
//  Budgie
//
//  Created by Emad A. on 22/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import Foundation
import CoreGraphics
import RxSwift

private enum SettingsKey: String {
    case format  = "USER_SETTINGS_FORMAT"
    case quality = "USER_SETTINGS_QUALITY"
    case sizing  = "USER_SETTINGS_SIZING"
}

struct SettingsViewModel {
    
    let format: Variable<PhotoFormat>
    let quality: Variable<PhotoQuality>
	
    let sizing: Variable<PhotoSizing>
	
    private let userDefaults: UserDefaults
    
    private let disposeBag = DisposeBag()
    
    // MARK: -
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
        
        //
        
        var photoFormatValue = PhotoFormat.jpg
        if let photoFormatRaw = userDefaults.string(forKey: SettingsKey.format.rawValue),
            let value = PhotoFormat(rawValue: photoFormatRaw)
        {
            photoFormatValue = value
        }
        format = Variable<PhotoFormat>(photoFormatValue)
        format.asObservable()
            .map { $0.rawValue }
            .subscribe(onNext: { value in
                userDefaults.set(value, forKey: SettingsKey.format.rawValue)
                userDefaults.synchronize()
            })
            .disposed(by: disposeBag)
        
        //
        
        var photoQualityValue = PhotoQuality.good
        let photoQualityRaw = CGFloat(userDefaults.double(forKey: SettingsKey.quality.rawValue))
        if let value = PhotoQuality(rawValue: photoQualityRaw) {
            photoQualityValue = value
        }
        quality = Variable<PhotoQuality>(photoQualityValue)
        quality.asObservable()
            .map { $0.rawValue }
            .subscribe(onNext: { value in
                userDefaults.set(value, forKey: SettingsKey.quality.rawValue)
                userDefaults.synchronize()
            })
            .disposed(by: disposeBag)
        
        //
        
        var photoSizingValue = PhotoSizing.none
        let PhotoSizingRaw = userDefaults.integer(forKey: SettingsKey.sizing.rawValue)
        if let saved = PhotoSizing(rawValue: PhotoSizingRaw) {
            photoSizingValue = saved
        }
        sizing = Variable<PhotoSizing>(photoSizingValue)
        sizing.asObservable()
            .map { $0.rawValue }
            .subscribe(onNext: { value in
                userDefaults.set(value, forKey: SettingsKey.sizing.rawValue)
                userDefaults.synchronize()
            })
            .disposed(by: disposeBag)
    }

}

enum PhotoFormat: String {
    case jpg = "JPG"
    case png = "PNG"
}

enum PhotoQuality: CGFloat {
    case best = 1.00
    case good = 0.95
    case fine = 0.85
}

enum PhotoSizing: Int {
	case none = 0
	case small = 800
	case medium = 1920
	case large = 4000
	
	var description: String {
		switch self {
		case .none: return "Do not resize"
		case .small, .medium, .large:
			return "\(self.rawValue) px"
		}
	}
}
