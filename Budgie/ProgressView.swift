//
//  CircularProgressView.swift
//  Budgie
//
//  Created by Emad A on 3/4/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class CircularProgressView: UIView {
	
	private let trackLayer: CAShapeLayer = {
		let layer = CAShapeLayer()
		layer.lineWidth = 2
		layer.fillColor = nil
		layer.strokeColor = UIColor(white: 1, alpha: 0.3).cgColor
		
		return layer
	}()
	
	fileprivate let progressLayer: CAShapeLayer = {
		let layer = CAShapeLayer()
		layer.lineWidth = 2
		layer.strokeEnd = 0
		layer.fillColor = nil
		layer.lineCap = kCALineCapRound
		layer.strokeColor = UIColor.white.cgColor
		
		return layer
	}()
	
	fileprivate let tickView = UIImageView(image: #imageLiteral(resourceName: "icon_tick"))
	
	// MARK: -
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		layer.addSublayer(trackLayer)
		layer.addSublayer(progressLayer)
		
		addSubview(tickView)
		tickView.isHidden = true
		tickView.autoAlignAxis(toSuperviewMarginAxis: .vertical)
		tickView.autoAlignAxis(toSuperviewMarginAxis: .horizontal)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func layoutSublayers(of layer: CALayer) {
		super.layoutSublayers(of: layer)
		guard layer == self.layer else { return }
		
		let trackLayerInset = trackLayer.lineWidth / 2
		trackLayer.frame = bounds.insetBy(dx: trackLayerInset, dy: trackLayerInset)
		trackLayer.path = UIBezierPath(ovalIn: trackLayer.bounds).cgPath
		
		progressLayer.frame = trackLayer.frame
		progressLayer.path = UIBezierPath(
			arcCenter: CGPoint(x: progressLayer.bounds.midX, y: progressLayer.bounds.midX),
			radius: progressLayer.bounds.size.width / 2,
			startAngle: (.pi * -0.5),
			endAngle: (.pi * -0.5) + (.pi * 2),
			clockwise: true).cgPath
	}
	
	func completed() {
		tickView.alpha = 0
		tickView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
		
		tickView.isHidden = false
		UIView.animate(
			withDuration: 0.2,
			delay: 0,
			options: .curveEaseOut,
			animations: { [unowned view = tickView] in
				view.alpha = 1
				view.transform = .identity
			},
			completion: nil)
	}
	
}

extension Reactive where Base: CircularProgressView {
	
	var progress: UIBindingObserver<Base, CGFloat> {
		return UIBindingObserver(UIElement: self.base, binding: { (view, progress) in
			let animation = CABasicAnimation(keyPath: "strokeEnd")
			animation.duration = 0.3
			animation.isRemovedOnCompletion = false
			animation.fillMode = kCAFillModeForwards
			animation.toValue = max(min(progress, 1), 0)
			animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
			
			view.progressLayer.add(animation, forKey: "")
		})
	}
	
}
