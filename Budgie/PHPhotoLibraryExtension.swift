//
//  PHPhotoLibraryExtension.swift
//  Budgie
//
//  Created by Emad A. on 24/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import Foundation
import RxSwift
import Photos

extension Reactive where Base: PHPhotoLibrary {
    
    func save(image: UIImage) -> Observable<PHAsset> {
        return Observable.create({ observer -> Disposable in
			// To have a reference of saved image.
            var placeholder: PHObjectPlaceholder?
			// Start performing changes ...
            PHPhotoLibrary.shared().performChanges({ 
                let request = PHAssetChangeRequest.creationRequestForAsset(from: image)
                // TODO: Set location of the slice, from the original photo
                // request.location = CLLocation(latitude: -33.903135, longitude: 151.245237)
                placeholder = request.placeholderForCreatedAsset
            }, completionHandler: { done, error in
                if let error = error {
                    observer.onError(error)
                    return
                } else if let placeholder = placeholder, done == true {
                    let options = PHFetchOptions()
                    options.predicate = NSPredicate(format: "localIdentifier = %@", placeholder.localIdentifier)
					if let asset = PHAsset.fetchAssets(with: options).firstObject {
						observer.onNext(asset)
						observer.onCompleted()
					}
                }
            })
            
            return Disposables.create()
        })
    }
    
    func save(images: [UIImage]) -> Observable<PHAsset> {
        var observables = [Observable<PHAsset>]()
        for image in images.reversed() {
            observables.append(self.save(image: image))
        }
        return Observable.concat(observables)
    }
	
	func move(assets: [PHAsset], toAlbumWithTitle title: String, identifier: String?) -> Observable<Void> {
		return self.find(album: title, identifier: identifier)
			.catchError { error -> Observable<PHAssetCollection> in
				return self.create(album: title)
					.catchErrorJustComplete()
			}
			.flatMap { collection in
				return Observable.create { observer -> Disposable in
					PHPhotoLibrary.shared().performChanges({
						PHAssetCollectionChangeRequest(for: collection)?.addAssets(assets as NSArray)
					}, completionHandler: { done, error in
                        if let error = error {
                            observer.onError(error)
                            return
                        }
						observer.onNext()
						observer.onCompleted()
					})
					
					return Disposables.create()
				}
			}
	}
	
	func find(album title: String, identifier: String?) -> Observable<PHAssetCollection> {
		return Observable.create({ observer -> Disposable in
			let options = PHFetchOptions()
            if let localIdentifier = identifier {
                options.predicate = NSPredicate(format: "localIdentifier = %@", localIdentifier)
            } else {
                options.predicate = NSPredicate(format: "title = %@", title)
            }
			
			if let album = PHAssetCollection
				.fetchAssetCollections(with: .album, subtype: .any, options: options)
				.firstObject
			{
				observer.onNext(album)
				observer.onCompleted()
			} else {
				let error = PHAssetCollectionError.notFound(title: title, identifier: identifier)
				observer.onError(error)
			}
			
			return Disposables.create()
		})
	}
	
	func create(album title: String) -> Observable<PHAssetCollection> {
		return Observable.create({ observer -> Disposable in
			var placeholder: PHObjectPlaceholder?
			PHPhotoLibrary.shared().performChanges({ 
				placeholder = PHAssetCollectionChangeRequest
					.creationRequestForAssetCollection(withTitle: title)
					.placeholderForCreatedAssetCollection
			}, completionHandler: { done, error in
				if let error = error {
					observer.onError(error)
				} else if let placeholder = placeholder, done == true {
					UserDefaults.standard.set(placeholder.localIdentifier, forKey: albumLocalIdentifier)
					UserDefaults.standard.synchronize()
					
					if let album = PHAssetCollection
						.fetchAssetCollections(withLocalIdentifiers: [placeholder.localIdentifier], options: nil)
						.firstObject
					{
						observer.onNext(album)
						observer.onCompleted()
					} else {
						let error = PHAssetCollectionError.notFound(title: title, identifier: placeholder.localIdentifier)
						observer.onError(error)
					}
				}
			})
			
			return Disposables.create()
		})
	}
	
}

let albumLocalIdentifier = "ALBUM_LOCAL_IDENTIFIER"

enum PHAssetError: Error {
	case notFound(identifier: String)
}

enum PHAssetCollectionError: Error {
	case notFound(title: String, identifier: String?)
}
