//
//  MainViewController.swift
//  Budgie
//
//  Created by Emad A. on 6/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxSwiftExt

class MainViewController: BaseViewController {
    
    lazy var viewModel: MainViewModel = {
        let settings = SettingsViewModel(userDefaults: UserDefaults.standard)
        return MainViewModel(settings: settings)
    }()
	
	// MARK: - View Properties
	
    private let scrollView = UIScrollView()
	private let contentView: UIStackView = {
		let view = UIStackView()
		view.axis = .vertical
		
		return view
	}()
    
    private let previewView = PreviewView()
	private let pageControl: PageControl = {
		let view = PageControl()
		view.currentPageIndicatorTintColor = UIColor(0x4990E2)
		view.pageIndicatorTintColor = UIColor(white: 0.85, alpha: 1)
		view.hidesForSinglePage = true
		
		return view
	}()
	
	private lazy var settingsTableView: SettingsTableView = {
		let view = SettingsTableView(viewModel: self.viewModel.settings)
		return view
	}()
    
	// MARK: - Private Properties
	
    private let disposeBag = DisposeBag()
    
    // MARK: - Overriden Methods
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
        self.title = "Preview"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
		
		// Add preview view to stack view.
        contentView.addArrangedSubview(previewView)
		
		// Add page control to stack view.
		// Add that in a container view so that it can be aligned center.
		let pageControlContainer = UIView()
		pageControlContainer.layoutMargins = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
		pageControlContainer.addSubview(pageControl)
        contentView.addArrangedSubview(pageControlContainer)
		
		// Add settings table view to stack view.
		contentView.addArrangedSubview(settingsTableView)
		
		// Add stack view in scroll view.
        scrollView.addSubview(contentView)
		
		// Add scroll view in view.
        view.addSubview(scrollView)
        
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		// Set navigation bar left/right buttons
		configureButtonBarItems()
		// Bind preview view and view model properties together.
        configurePreviewView()
		// Do proper thing on chaging settings.
		configureSettingsView()
		
        // Needs to observe the width of preview view showing the photo.
        // That width basicaly creates the size of each slice.
        previewView.rx.observeWeakly(CGRect.self, "bounds")
            .unwrap()
            .map({ $0.size })
            .bind(to: viewModel.sliceSize)
            .disposed(by: disposeBag)
    }
	
    override func setupViewConstraints() {
        super.setupViewConstraints()
		
        contentView.autoPinEdgesToSuperviewEdges()
        contentView.autoMatch(.width, to: .width, of: scrollView)
		
		scrollView.autoPinEdgesToSuperviewEdges()
		
		previewView.autoMatch(.height, to: .width, of: previewView)
		
		pageControl.autoAlignAxis(toSuperviewAxis: .vertical)
		pageControl.autoPinEdge(toSuperviewMargin: .bottom)
		pageControl.autoPinEdge(toSuperviewMargin: .top)
    }
    
    // MARK: - Private Methods
	
	private func configureButtonBarItems() {
		let saveBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: nil, action: nil)
		saveBarButtonItem.rx.tap
			.subscribe(onNext: { [weak self] in self?.presentSavingSlices() })
			.disposed(by: disposeBag)
		navigationItem.rightBarButtonItem = saveBarButtonItem
	}
	
    private func configurePreviewView() {
        // To update the page indicator
        // based on value coming from preview view.
        previewView.rx.numberOfPages
            .bind(to: pageControl.rx.numberOfPages)
            .disposed(by: disposeBag)
        
        // To update the page indicator
        // based on value coming from preview view.
        previewView.rx.currentPage
            .bind(to: pageControl.rx.currentPage)
            .disposed(by: disposeBag)
        
        // Preview view is listening to model view for photo.
        viewModel.preview.asDriver()
            .drive(onNext: { [weak self] photo in
                self?.previewView.rx.photo.onNext(photo)
                self?.navigationItem.rightBarButtonItem?.isEnabled = photo != nil
            })
            .disposed(by: disposeBag)
    }
	
	private func configureSettingsView() {
		// Show proper view controller on settings item selection.
		settingsTableView.rx.itemSelected
			.map { $0.row }
			.map { [settings = self.viewModel.settings] index -> UIViewController? in
				if index == 0 {
					return SettingsPhotoFormatQualityViewController(viewModel: settings)
				} else if index == 1 {
					return SettingsPhotoResizingViewController(viewModel: settings)
				}
				return nil
			}
			.unwrap()
			.subscribe(onNext: { [weak self] vc in
				self?.navigationController?.pushViewController(vc, animated: true)
			})
			.disposed(by: disposeBag)
	}
	
	private func presentSavingSlices() {
		guard let photo = viewModel.photo.value else { return }
        
		let actionViewController = SaveActionViewController(
			photo: photo,
			numberOfSlices: pageControl.numberOfPages,
			sizing: viewModel.settings.sizing.value,
			format: viewModel.settings.format.value,
			quality: viewModel.settings.quality.value)
		// Define the action will be done on starting over.
		actionViewController.dismiss = { [weak navigationController] in
			navigationController?.popToRootViewController(animated: false)
		}
		
		present(actionViewController, animated: true, completion: nil)
	}
	
}
