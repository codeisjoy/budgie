//
//  SettingsPhotoFormatQualityViewController.swift
//  Budgie
//
//  Created by Emad A. on 24/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit

private struct Option {
    
    let format: PhotoFormat
    let quality: PhotoQuality
    
}

final class SettingsPhotoFormatQualityViewController: SettingsTableViewController {
    
    private let viewModel: SettingsViewModel
    
    private let options: [Option] = [
        Option(format: .jpg, quality: .best),
        Option(format: .jpg, quality: .good),
        Option(format: .jpg, quality: .fine),
        Option(format: .png, quality: .best)]
    
    private let reusableCellId = "OptionCell"
    
    // MARK: -
    
    init(viewModel: SettingsViewModel) {
        self.viewModel = viewModel
        super.init(style: .grouped)
        title = "Format and quality"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SettingsCell.self, forCellReuseIdentifier: reusableCellId)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableCellId, for: indexPath)
        let option = options[indexPath.row]
        
        var text = option.format.rawValue
        if option.format == .jpg {
            text.append(" \(Int(option.quality.rawValue * 100))%")
        }
        cell.textLabel?.text = text
        
        if option.format == viewModel.format.value && option.quality == viewModel.quality.value {
            selectedSettingsIndexPath.value = indexPath
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return
            "Choose the fortmat and compression rate that will be applied when exporting an image.\n\n" +
            "JPG 100% will apply the lowest compression rate at the cost of increased file size."
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        
        selectedSettingsIndexPath.value = indexPath
        
        let option = options[indexPath.row]
        viewModel.format.value = option.format
        viewModel.quality.value = option.quality
        
        _ = navigationController?.popViewController(animated: true)
    }
    
}
