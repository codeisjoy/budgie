//
//  PageControl.swift
//  Budgie
//
//  Created by Emad A. on 12/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit

final class PageControl: UIPageControl {
    
    private let spacing: CGFloat = 3
    private let dotSize = CGSize(width: 6, height: 6)
    
    override var intrinsicContentSize: CGSize {
        return size(forNumberOfPages: numberOfPages)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for (index, view) in subviews.enumerated() {
            view.frame = CGRect(
                origin: CGPoint(
                    x: bounds.minX + (CGFloat(index) * (dotSize.width + spacing)),
                    y: bounds.minY),
                size: dotSize)
        }
    }
    
    override func size(forNumberOfPages pageCount: Int) -> CGSize {
        return CGSize(
            width: (CGFloat(pageCount) * dotSize.width) + (CGFloat(pageCount - 1) * spacing),
            height: dotSize.height)
    }
    
}
