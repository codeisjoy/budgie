//
//  RoundedButton.swift
//  Budgie
//
//  Created by Emad A on 3/4/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit

final class RoundedButton: UIButton {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = .white
		setTitleColor(UIColor(0x4A4A4A), for: .normal)
		contentEdgeInsets = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)
	}
	
	override func setTitle(_ title: String?, for state: UIControlState) {
		super.setTitle(title, for: state)
		titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func layoutSublayers(of layer: CALayer) {
		super.layoutSublayers(of: layer)
		if layer == self.layer {
			layer.cornerRadius = min(layer.bounds.width, layer.bounds.height) / 2
		}
	}
	
	override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
		var rect = super.titleRect(forContentRect: contentRect)
		rect.origin.x = contentRect.minX
		
		return rect
	}
	
	override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
		var rect = super.imageRect(forContentRect: contentRect)
		rect.origin.x = contentRect.maxX - rect.width
		
		return rect
	}
	
}
