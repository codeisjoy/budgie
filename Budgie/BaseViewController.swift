//
//  BaseViewController.swift
//  Budgie
//
//  Created by Emad A. on 6/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import RxSwift
import RxSwiftExt
import PureLayout

class BaseViewController: UIViewController {

    private var didSetupConstraints: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.setNeedsUpdateConstraints()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func updateViewConstraints() {
        if didSetupConstraints == false {
            setupViewConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    func setupViewConstraints() {
        // To be overriden
    }
    
}

class BaseTableViewController: UITableViewController {
    
    let disposeBag = DisposeBag()
    
    private var feedbackGenerator: UISelectionFeedbackGenerator?
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
        tableView.backgroundColor = UIColor(0xFAFAFA)
        tableView.separatorColor = UIColor(0xD0D0D0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor(0x898989)
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightRegular)
        
        label.text = tableView.dataSource?.tableView?(tableView, titleForFooterInSection: section)
        
        let view = UIView()
        view.addSubview(label)
        
        label.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(
            top: 20,
            left: tableView.separatorInset.left,
            bottom: 20,
            right: tableView.separatorInset.left))
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        feedbackGenerator = UISelectionFeedbackGenerator()
        feedbackGenerator?.prepare()
        
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        feedbackGenerator?.selectionChanged()
    }
    
}

class SettingsTableViewController: BaseTableViewController {
    
    let selectedSettingsIndexPath = Variable<IndexPath?>(nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedSettingsIndexPath.asObservable()
            .unwrap()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak tableView = self.tableView] indexPath in
                tableView?.visibleCells.forEach { $0.accessoryType = .none }
                tableView?.cellForRow(at: indexPath)?.accessoryType = .checkmark
            })
            .disposed(by: disposeBag)
    }
    
}
