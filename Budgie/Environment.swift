//
//  Environment.swift
//  Budgie
//
//  Created by Emad A on 13/4/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import Foundation

final class Environment {
	
	static let current = Environment()
	
	lazy var configuration: EnvConfig = {
		guard let env = self.data?["environment"] as? String else {
			return .unknown
		}
		return EnvConfig(rawValue: env) ?? .unknown
	}()
	
	public lazy var bundleVersion: String? = {
		return Bundle.main.infoDictionary?["CFBundleVersion"] as? String
	}()
	
	// MARK: - Private Properties
	
	private var data: [String: Any]?
	
	// MARK: - Overriden Methods
	
	private init() {
		guard let resource = Bundle.main.path(forResource: "Environment", ofType: "plist"),
			let plist = FileManager.default.contents(atPath: resource) else
		{
			print("Error reading enrivonment plist file.")
			return
		}
		var format = PropertyListSerialization.PropertyListFormat.xml
		do {
			data = try PropertyListSerialization.propertyList(
				from: plist,
				options: .mutableContainersAndLeaves,
				format: &format) as? [String: Any]
		} catch {
			print("Error reading plist \(error)")
		}
	}
	
}

// MARK: - EnvConfig Enum
// MARK: -

enum EnvConfig: String {
	
	case unknown = "Unknown"
	case debug	 = "Debug"
	case release = "Release"
	
}
