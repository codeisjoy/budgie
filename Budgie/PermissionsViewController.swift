//
//  PermissionsViewController.swift
//  Budgie
//
//  Created by Emad A on 6/4/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import Photos

final class PermissionsViewController: BaseViewController {
	
    // MARK: - Overriden Methods
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Private Methods
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = .center
        label.text = "Vawin requires camera roll permissions"
        label.font = UIFont.systemFont(ofSize: 26, weight: UIFontWeightLight)
        
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        label.text =
            "Accessing the Photo library needs your explicit permission. " +
            "It is required for selecting and editing photos.\n\n" +
            "No photo will be sent out of your device or shared without your approval."
        
        return label
    }()
    
    private let actionButton: UIButton = {
        let btn = RoundedButton(type: .system)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.white.cgColor
        btn.setTitle("Give Permission", for: .normal)
        btn.setImage(#imageLiteral(resourceName: "icon_arrow"), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = .clear
        btn.tintColor = .white
        
        return btn
    }()
    
    // MARK: - Overriden Methods
    
	override func loadView() {
		let view = UIView()
		view.backgroundColor = .black
		
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        
        view.addSubview(actionButton)
        
		self.view = view
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonAction()
    }
    
    override func setupViewConstraints() {
        super.setupViewConstraints()
        
        titleLabel.autoSetDimension(.width, toSize: 280)
        titleLabel.autoAlignAxis(toSuperviewAxis: .vertical)
        titleLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 120)
        
        descriptionLabel.autoAlignAxis(toSuperviewAxis: .vertical)
        descriptionLabel.autoMatch(.width, to: .width, of: titleLabel)
        descriptionLabel.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 50)
        
        actionButton.autoSetDimension(.width, toSize: 200)
        actionButton.autoAlignAxis(toSuperviewAxis: .vertical)
        actionButton.autoPinEdge(toSuperviewEdge: .bottom, withInset: 80)
    }
    
    // MARK: - Private Methods
    
    private func setButtonAction() {
        let selector = PHPhotoLibrary.authorizationStatus() == .notDetermined ?
            #selector(requestAuthorization) : #selector(openSettings)
        
        actionButton.addTarget(self, action: selector, for: .touchUpInside)
    }
    
    @objc private func requestAuthorization() {
        PHPhotoLibrary.requestAuthorization {[weak self] status in
            if status == .authorized {
                self?.dismiss(animated: true, completion: nil)
            } else {
                self?.setButtonAction()
            }
        }
    }
    
    @objc private func openSettings() {
        guard let settingsURL = URL(string: UIApplicationOpenSettingsURLString) else { return }
        UIApplication.shared.open(settingsURL)
    }
}
