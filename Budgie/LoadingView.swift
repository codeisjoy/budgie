//
//  LoadingView.swift
//  Budgie
//
//  Created by Emad A on 3/4/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class LoadingView: UIView {
	
    override var tintColor: UIColor! {
        didSet {
			progressLayer.strokeColor = tintColor.cgColor
			trackLayer.strokeColor = tintColor.withAlphaComponent(0.3).cgColor
		}
    }
    
	private let trackLayer: CAShapeLayer = {
		let layer = CAShapeLayer()
		layer.fillColor = nil
		layer.lineWidth = 2
		
		return layer
	}()
	
	fileprivate let progressLayer: CAShapeLayer = {
		let layer = CAShapeLayer()
		layer.lineCap = kCALineCapRound
		layer.fillColor = nil
        layer.lineWidth = 2
        
		return layer
	}()
	
    fileprivate let tickView = UIImageView(image: #imageLiteral(resourceName: "icon_tick"))
	
	fileprivate var shouldComplete: Bool = false
	fileprivate let didComplete = PublishSubject<Void>()
	
    // MARK: - Animations Properties
    
	private var movingStrokeStartAnimation: CAAnimation {
		let animation = CAKeyframeAnimation(keyPath: "strokeStart")
		animation.keyTimes = [0, 0.2, 1]
		animation.values = [0, 0, 1]
        
		return animation
	}
	
	private var movingStrokeEndAnimation: CAAnimation {
		let animation = CAKeyframeAnimation(keyPath: "strokeEnd")
		animation.keyTimes = [0, 0.8, 1]
		animation.values = [0, 1, 1]
        
		return animation
	}
	
    private var completingStrokeEndAnimation: CAAnimation {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        
        return animation
    }
    
	// MARK: -
	
	override init(frame: CGRect) {
		super.init(frame: frame)
        
		layer.addSublayer(trackLayer)
		trackLayer.strokeColor = tintColor.withAlphaComponent(0.3).cgColor
		
		progressLayer.strokeEnd = 0
		progressLayer.strokeStart = 0
		layer.addSublayer(progressLayer)
		progressLayer.strokeColor = tintColor.cgColor
		
        addSubview(tickView)
        tickView.isHidden = true
        tickView.autoAlignAxis(toSuperviewMarginAxis: .vertical)
        tickView.autoAlignAxis(toSuperviewMarginAxis: .horizontal)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
    
	override func layoutSublayers(of layer: CALayer) {
		super.layoutSublayers(of: layer)
		guard layer == self.layer else { return }
		
		let trackLayerInset = trackLayer.lineWidth / 2
		trackLayer.frame = bounds.insetBy(dx: trackLayerInset, dy: trackLayerInset)
		trackLayer.path = UIBezierPath(ovalIn: trackLayer.bounds).cgPath
		
		progressLayer.frame = trackLayer.frame
		progressLayer.path = UIBezierPath(
			arcCenter: CGPoint(x: progressLayer.bounds.midX, y: progressLayer.bounds.midX),
			radius: progressLayer.bounds.size.width / 2,
			startAngle: (.pi * -0.5),
			endAngle: (.pi * -0.5) + (.pi * 2),
			clockwise: true).cgPath
	}
	
	func start() {
		let animations = CAAnimationGroup()
        animations.timingFunction = CAMediaTimingFunction(controlPoints: 0.7, 0, 0.3, 1)
		animations.fillMode = kCAFillModeForwards
        animations.isRemovedOnCompletion = false
        animations.delegate = self
        animations.duration = 1.5
        
        if shouldComplete == true {
            animations.animations = [completingStrokeEndAnimation]
        } else {
            animations.animations = [movingStrokeStartAnimation, movingStrokeEndAnimation]
        }
        
        guard progressLayer.animation(forKey: "progress") == nil else {
            return
        }
        
        isHidden = false
		progressLayer.add(animations, forKey: "progress")
	}
	
    func stop() {
        isHidden = true
        progressLayer.removeAllAnimations()
    }
    
    /// After calling this method loading will complete on the next round.
    func willComplete() {
        shouldComplete = true
    }
    
}

extension LoadingView: CAAnimationDelegate {
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
		guard let animations = (anim as? CAAnimationGroup)?.animations, flag == true else { return }
		
		if animations.count > 1 {
			progressLayer.removeAllAnimations()
			start()
		} else {
			tickView.alpha = 0
			tickView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
			
			tickView.isHidden = false
			UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.2, delay: 0, options: .curveEaseOut,
				animations: { [unowned tickView] in
					tickView.alpha = 1
					tickView.transform = .identity
				}, completion: nil)
			
			didComplete.onNext()
		}
    }
    
}

extension Reactive where Base: LoadingView {
	
	var didComplete: ControlEvent<Void> {
		return ControlEvent(events: self.base.didComplete)
	}
	
}
