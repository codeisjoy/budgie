//
//  SaveActionViewModel.swift
//  Budgie
//
//  Created by Emad A on 29/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import Photos
import RxSwift
import Mixpanel

struct SaveActionViewModel {
	
	/// The original photo.
	let photo: UIImage
	// The number of slices to save.
	let count: Int
	// The output Settings.
	let settings: (sizing: PhotoSizing, format: PhotoFormat, quality: PhotoQuality)
	
	let completion: () -> ()
    let presentError: (Error) -> ()
	
	// MARK - Private Methods
	
    private var dispatchTime: DispatchTime?
    
    private let disposeBag = DisposeBag()
    
    // MARK -
    
	init(photo: UIImage,
	     numberOfSlices count: Int,
	     sizing: PhotoSizing, format: PhotoFormat, quality: PhotoQuality,
	     completion: @escaping () -> (),
	     presentError: @escaping (Error) -> ())
	{
		self.photo = photo
		self.count = count
		
		self.settings = (sizing: sizing, format: format, quality: quality)
		
		self.completion = completion
        self.presentError = presentError
	}
    
    mutating func export() {
        guard let photo = photo.resize(height: CGFloat(settings.sizing.rawValue)) else {
            return
        }
        
        dispatchTime = .now()
        
        let dimension = min(photo.size.width, photo.size.height)
        let sliceSize = CGSize(width: dimension, height:dimension)
        
        var slices = [UIImage]()
        for i in 0..<count {
            let rect = CGRect(
                origin: CGPoint(x: sliceSize.width * CGFloat(i), y: 0),
                size: sliceSize)
			if let slice = photo
				.crop(rect: rect, scale: 1)?
				.compress(format: settings.format, quality: settings.quality)
			{
                slices.append(slice)
            }
        }
        
		save(slices)
    }
	
	private func save(_ slices: [UIImage]) {
		var assets = [PHAsset]()
		PHPhotoLibrary.shared().rx.save(images: slices)
			.subscribe(
				onNext: { asset in assets.append(asset) },
				onError: presentError,
				onCompleted: { self.move(assets, toAlbume: "Vawin") })
			.disposed(by: disposeBag)
	}
	
	private func move(_ assets: [PHAsset], toAlbume albume: String) {
		let albumLocalId = UserDefaults.standard.string(forKey: albumLocalIdentifier)
		PHPhotoLibrary.shared().rx.move(assets: assets, toAlbumWithTitle: albume, identifier: albumLocalId)
			.observeOn(MainScheduler.instance)
			.subscribe(
				onError: presentError,
				onCompleted: {
                    self.completion()
					if let start = self.dispatchTime?.uptimeNanoseconds {
						let duration = (DispatchTime.now().uptimeNanoseconds - start) / 1_000_000_000
						Mixpanel.sharedInstance()?.track("Export", properties: [
							"Duration": duration,
							"Number Of Slices": assets.count])
					}
            })
			.disposed(by: self.disposeBag)
	}
    
}
