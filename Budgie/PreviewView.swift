//
//  PreviewView.swift
//  Budgie
//
//  Created by Emad A. on 12/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class PreviewView: UIView {
    
    fileprivate let scrollView = UIScrollView()
    fileprivate let loadingView = LoadingView()
    fileprivate let imageView = UIImageView()
    
    private let disposeBag = DisposeBag()
    
    private var didSetupConstraints = false
    
    // MARK: - Overriden Methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        loadingView.tintColor = .black
        addSubview(loadingView)
        
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.addSubview(imageView)
        addSubview(scrollView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        if didSetupConstraints == false {
            loadingView.autoSetDimensions(to: CGSize(width: 30, height: 30))
            loadingView.autoAlignAxis(toSuperviewAxis: .horizontal)
            loadingView.autoAlignAxis(toSuperviewAxis: .vertical)
            
            imageView.autoPinEdgesToSuperviewEdges()
            scrollView.autoPinEdgesToSuperviewEdges()
            
            didSetupConstraints = true
        }
        super.updateConstraints()
    }
	
}

extension Reactive where Base: PreviewView {
    
    /// Bindable photo will be shown.
    var photo: UIBindingObserver<Base, UIImage?> {
        return UIBindingObserver(UIElement: self.base, binding: { (view, photo) in
			view.scrollView.setContentOffset(.zero, animated: false)
            photo == nil ? view.loadingView.start() : view.loadingView.stop()
            UIView.transition(
                with: view, duration: 0.1, options: .transitionCrossDissolve,
                animations: { view.imageView.image = photo },
                completion: nil)
        })
    }
    
    /// Returns number of pages to scroll, depending on photo size.
    var numberOfPages: Observable<Int> {
        return self.base.imageView.rx.observeWeakly(UIImage.self, "image")
            .asObservable()
            .map { [weak view = self.base] photo in
                guard let photoWidth = photo?.size.width,
                    let width = view?.bounds.width, width > 0
                else { return 0 }
                return Int(ceil(photoWidth) / width)
            }
    }
    
    /// Returns the current page number,
    /// when photo is wide enough for paging to apply.
    var currentPage: Observable<Int> {
        return self.base.scrollView.rx.didScroll
            .throttle(0.3, latest: true, scheduler: MainScheduler.asyncInstance)
            .map { [weak scrollView = self.base.scrollView] in
                guard let scrollView = scrollView else { return 0 }
                let x = scrollView.contentOffset.x
                let w = scrollView.bounds.width
                return Int(round(x / w))
            }
    }
    
}
