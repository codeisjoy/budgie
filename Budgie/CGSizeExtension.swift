//
//  CGSizeExtension.swift
//  Budgie
//
//  Created by Emad A. on 22/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import CoreGraphics

extension CGSize {
    
    var isAnyDimentionZero: Bool {
        return width == 0 || height == 0
    }
    
    func by(_ x: CGFloat) -> CGSize {
        return CGSize(width: width * x, height: height * x)
    }
}
