//
//  RxExtension.swift
//  Budgie
//
//  Created by Emad A. on 22/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import RxSwift

extension Variable {

    func asObservableVoid() -> Observable<Void> {
        return self.asObservable().map { _ in }
    }
    
}
