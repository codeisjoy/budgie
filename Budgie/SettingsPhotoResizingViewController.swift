//
//  SettingsPhotoResizingViewController.swift
//  Budgie
//
//  Created by Emad A. on 24/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit

class SettingsPhotoResizingViewController: SettingsTableViewController {
    
    private let viewModel: SettingsViewModel
    
    private let options: [PhotoSizing] = [.none, .small, .medium, .large]
    
    private let reusableCellId = "OptionCell"
    
    // MARK: -
    
    init(viewModel: SettingsViewModel) {
        self.viewModel = viewModel
        super.init(style: .grouped)
        title = "Image sizing"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SettingsCell.self, forCellReuseIdentifier: reusableCellId)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableCellId, for: indexPath)
        
        let option = options[indexPath.row]
        cell.textLabel?.text = option.description
        
        if option == viewModel.sizing.value {
            selectedSettingsIndexPath.value = indexPath
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return
            "Choose the minimum image size for exporting. The size given refer to the long edge of the image " +
            "(for example, to the width for landscape images) and would be only apply to images larger than selected " +
            "value. Smaller images will not be enlarged."
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        
        selectedSettingsIndexPath.value = indexPath
        
        let option = options[indexPath.row]
        viewModel.sizing.value = option
        
        _ = navigationController?.popViewController(animated: true)
    }
    
}
