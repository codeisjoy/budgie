//
//  UIImageExtension.swift
//  Budgie
//
//  Created by Emad A on 3/4/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit

extension UIImage {
	
	/**
	Returns a new image by cropping the image in provided area.
	- Parameters:
		- rect: The rectangle should be cropped.
		- scale: The scale factor to resize after cropping.
	- Returns: A subimage in given area. Or `nil` if can not complete the task.
	*/
	func crop(rect: CGRect, scale: CGFloat) -> UIImage? {
		guard let cgImage = cgImage?.cropping(to: rect) else {
			return nil
		}
        
        return UIImage(cgImage: cgImage, scale: 1 / scale, orientation: imageOrientation)
	}
	
	/**
	Scales down provided photo to match the height with given dimention and returns it.
	- Parameters:
		- height: The height that resized photo should have. If photo's current height is less, it would not be enlarged.
	- Returns: A resized photo with height equal or less than provided number. Or `nil` if can not complete the task.
	*/
	func resize(height: CGFloat) -> UIImage? {
		let scale = min(size.height, height) / size.height
		// If scale is zero, it means no resize required. So, return the original image.
		guard scale > 0 else {
			return self
		}
		
		// The size given photo should be resized to.
		let resizedSize = CGSize(width: size.width * scale, height: size.height * scale)
		// Resize the image ...
		UIGraphicsBeginImageContext(resizedSize)
		draw(in: CGRect(origin: .zero, size: resizedSize))
		let resized = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return resized
	}
	
	/**
	Returns an image with given format and quality.
	- Parameters:
		- format: The format of output, either JPG or PNG.
		- quality: The compression quality of output.
	- Returns: An image with given format and quality. Or `nil` if can not complete the task.
	*/
	func compress(format: PhotoFormat, quality: PhotoQuality) -> UIImage? {
		let compressed: Data?
		switch format {
		case .jpg:
			compressed = UIImageJPEGRepresentation(self, quality.rawValue)
			break
		case .png:
			compressed = UIImagePNGRepresentation(self)
			break
		}
		
		guard let data = compressed else { return nil }
		return UIImage(data: data)
	}
    
    class func solid(color: UIColor) -> UIImage? {
        let scale = UIScreen.main.scale
        let rect = CGRect(x: 0, y: 0, width: 1 / scale, height: 1 / scale)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
