//
//  HomeViewController.swift
//  Budgie
//
//  Created by Emad A on 31/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit

final class HomeViewController: BaseViewController {
	
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = "Vawin your panos"
        label.textColor = UIColor(0x4A4A4A)
        label.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightSemibold)
        
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor(0x4A4A4A)
        label.text = "to share them on Instagram"
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular)
        
        return label
    }()
    
    private let vawinImageView = UIImageView(image: #imageLiteral(resourceName: "vawin"))
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = UIColor(0x666666)
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)
        label.text = "To get it started, select your photo. " +
            "Vawin will slice it in the best possible way. " +
            "Then share the slices on Instagram as a multi-photos post."
        
        return label
    }()
    
	private let openButton: UIButton = {
		let btn = UIButton(type: .system)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor(0x4990E2)
		btn.setTitle("Open Photo", for: .normal)
		btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightBold)
		
		return btn
	}()
	
	override func loadView() {
		let view = UIView()
		view.backgroundColor = .white
		
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        
        view.addSubview(vawinImageView)
        
        view.addSubview(textLabel)
        
        view.addSubview(openButton)
        
		self.view = view
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		openButton.addTarget(self, action: #selector(open), for: .touchUpInside)
    }
	
	override func setupViewConstraints() {
		super.setupViewConstraints()
        
        titleLabel.autoAlignAxis(toSuperviewAxis: .vertical)
        titleLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 45)
        
        subtitleLabel.autoAlignAxis(toSuperviewMarginAxis: .vertical)
        subtitleLabel.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 7)
        
        vawinImageView.autoAlignAxis(toSuperviewMarginAxis: .vertical)
        vawinImageView.autoPinEdge(.top, to: .bottom, of: subtitleLabel, withOffset: 40)
        
        textLabel.autoPinEdge(toSuperviewEdge: .leading, withInset: 40)
        textLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: 40)
        textLabel.autoPinEdge(.top, to: .bottom, of: vawinImageView, withOffset: 40)
		
		openButton.autoSetDimension(.height, toSize: 70)
        openButton.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
	}
	
	@objc private func open() {
		let imagePickerViewController = UIImagePickerController()
		imagePickerViewController.navigationBar.isTranslucent = false
		imagePickerViewController.allowsEditing = false
		imagePickerViewController.delegate = self
		
		present(imagePickerViewController, animated: true, completion: nil)
	}
	
}

extension HomeViewController: UINavigationControllerDelegate {
	
	func navigationController(
		_ navigationController: UINavigationController,
		willShow viewController: UIViewController,
		animated: Bool)
	{
		let backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
		viewController.navigationItem.backBarButtonItem = backBarButtonItem
	}
	
}

extension HomeViewController: UIImagePickerControllerDelegate {
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
			let mainViewController = MainViewController()
			mainViewController.viewModel.photo.value =  image
			navigationController?.pushViewController(mainViewController, animated: false)
		}
		
		picker.dismiss(animated: true, completion: nil)
	}
	
}
