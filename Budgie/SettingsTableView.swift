//
//  SettingsTableView.swift
//  Budgie
//
//  Created by Emad A on 23/3/17.
//  Copyright © 2017 Emad A. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class SettingsTableView: UITableView {
	
	private let viewModel: SettingsViewModel
    
	private let disposeBag = DisposeBag()
	
    fileprivate let reusableCellId = "SettingsRow"
    
	// MARK: -
	
	init(viewModel: SettingsViewModel) {
		self.viewModel = viewModel
		super.init(frame: .zero, style: .plain)
        
        delegate = self
        dataSource = self
        isScrollEnabled = false
        register(SettingsCell.self, forCellReuseIdentifier: reusableCellId)
	}
	
	required init(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
    
    // MARK: -
    
    fileprivate func configureFormatAndQualityCell(_ cell: SettingsCell?) {
        cell?.textLabel?.text = "Format and quality"
        cell?.imageView?.image = #imageLiteral(resourceName: "icon_format")
        guard let detailTextLabel = cell?.detailTextLabel else {
            return
        }
        
        Observable.combineLatest(
            viewModel.format.asObservable(),
            viewModel.quality.asObservable()) { format, quality in
                var output = format.rawValue
                if format == .jpg {
                    output.append(" \(Int(quality.rawValue * 100))%")
                }
                return output
            }
            .bind(to: detailTextLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    fileprivate func configureResizingOptionsCell(_ cell: SettingsCell?) {
        cell?.textLabel?.text = "Image resizing"
        cell?.imageView?.image = #imageLiteral(resourceName: "icon_sizing")
        guard let detailTextLabel = cell?.detailTextLabel else {
            return
        }
        
        viewModel.sizing.asObservable()
            .map { $0.description }
            .bind(to: detailTextLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
}

extension SettingsTableView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableCellId, for: indexPath)
        cell.layoutMargins = tableView.separatorInset
        
        let settingCell = cell as? SettingsCell
        if indexPath.row == 0 {
            configureFormatAndQualityCell(settingCell)
        } else if indexPath.row == 1 {
            configureResizingOptionsCell(settingCell)
        }
        
        return cell
    }
    
}

extension SettingsTableView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

final class SettingsCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.value1, reuseIdentifier: reuseIdentifier)
        textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        textLabel?.textColor = UIColor(0x4A4A4A)
        
        detailTextLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        detailTextLabel?.textColor = UIColor(0x4990E2)
        
        selectedBackgroundView = UIView()
        selectedBackgroundView?.backgroundColor = UIColor(0xEFEFEF)
        
        accessoryType = .disclosureIndicator
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
